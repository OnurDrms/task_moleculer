var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  username:  { type: String }, 
  password: { type: String },
  products: [{
    name: { type: String },
    price: { type: String }
  }]
});

module.exports = User = mongoose.model('User', userSchema,'User');