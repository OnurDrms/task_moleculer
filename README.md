Local dev environment using Docker, NodeJs, Redis, MongoDB, RabbitMQ , Moleculer

Instruction
You must have Docker to run this repo

Login 
http://localhost:3000/login with post method (body: {username:"username",password:"password"})

Logout
http://localhost:3000/logout with get method

Register 
http://localhost:3000/register with post method (body: {username:"username",password:"password"})

Add to Cart 
http://localhost:3000/addProduct with post method (body: {name:"name",price:"price"})

Show Cart 
http://localhost:3000/products with get method

Usage

Clone this repo
npm install
docker-compose up

