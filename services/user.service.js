"use strict";
const ApiGwService = require("moleculer-web");
const express = require("express");
var bodyParser = require('body-parser');
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var amqp = require('amqplib/callback_api');
var redis = require("redis");
var redisClient= redis.createClient({
    port      : 6379,
    host      : 'redis'
});
require('../db');
var User = require('../models/users');

redisClient.on('error', function(err) {
    console.log('Redis error: ' + err);
});

redisClient.on("ready",function () {
    console.log("Redis is ready");
});

const DbService = require("../mixins/db.mixin");
const CacheCleanerMixin = require("../mixins/cache.cleaner.mixin");

module.exports = {
	name: "users",
	mixins: [
		DbService("users"),
		CacheCleanerMixin([
			"cache.clean.users"
		])
	],

	settings: {
        mixins: [ApiGwService],

        port: 3000
	},


	
    created() {
		const app = express();
        app.use(bodyParser.urlencoded({ extended: false }))
        app.use(bodyParser.json())

        app.use(session({
            secret: 'mysecret',
            store: new redisStore({client: redisClient,ttl : 260}),
            saveUninitialized: false,
            resave: false
        }));
		this.initRoutes(app);

        redisClient.on('error', function(err) {
            console.log('Redis error: ' + err);
        });
        
        redisClient.on("ready",function () {
            console.log("Redis is ready");
        });
		this.app = app;
	},

	started() {
		this.app.listen(Number(this.settings.port), err => {
			if (err)
				return this.broker.fatal(err);

			this.logger.info(` server started on port ${this.settings.port}`);
		});

	},

	stopped() {
		if (this.app.listening) {
			this.app.close(err => {
				if (err)
					return this.logger.error(" server close error!", err);

				this.logger.info(" server stopped!");
			});
		}
	},
    
	/**
	 * Methods
	 */
	methods: {
        initRoutes(app) {
			app.post("/login", this.login);
			app.get("/logout", this.logout);
			app.post("/register", this.register);
			app.post("/addProduct", this.addProduct);
			app.get("/products", this.products);
        },
         register(req,res) {
			
				let user = req.body;
                amqp.connect('amqp://rabbitmq', function(error0, connection) {
                    if (error0) {
                        throw error0;
                    }
                    connection.createChannel(function(error1, channel) {
                        if (error1) {
                        throw error1;
                        }
                        var queue = 'register';
                        var msg = user;
            
                        channel.assertQueue(queue, {
                        durable: false
                        });
            
                        channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
                        
                        channel.consume(queue, function(msg) {
                            User.create({username:JSON.parse(msg.content.toString()).username,password:JSON.parse(msg.content.toString()).password},function (err, doc) {
                                if (err) return err;
                                res.json({status:"Registration Successful"}); 
                            })              
                        }, {
                            noAck: true
                        });
                    });
                });
			
		},

		 login(req,res) {
			
                
				const { username, password } = req.body;

				const user =  this.adapter.findOne({ username,password });
                amqp.connect('amqp://rabbitmq', function(error0, connection) {
                    if (error0) {
                        throw error0;
                    }
                    connection.createChannel(function(error1, channel) {
                        if (error1) {
                        throw error1;
                        }
                        var queue = 'login';
                        var msg = user;
            
                        channel.assertQueue(queue, {
                        durable: false
                        });
            
                        channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
                        
                        channel.consume(queue, function(msg) {
                            if (redisClient.connected) {
                                //Redis Search
                                redisClient.keys('User*', function (err, keys) {
                                    if (err) return console.log(err);
                         
                                    if (keys.length > 0) {
                                        for (var i = 0, len = keys.length; i < len; i++) {
                                            redisClient.get(keys[i], function (err, user) {                        
                                                if (JSON.parse(msg.content.toString()).username === JSON.parse(user).username) {
                                                    req.session.userId = JSON.parse(user)._id;
                                                }else{
                                                    //There is no record on Redis.
                                                    var counter = 0;
                                                    User.find({},function (err, doc) {
                                                        if(doc.length > 0){
                                                            doc.forEach(function (item) {
                                                                counter += 1;
                                                                //Write All Data To Redis
                                                                var userName = 'User' + counter;
                                                                redisClient.get(userName, function (err, user) {
                                                                    if (user == null) {
                                                                        var data = JSON.stringify(item._doc);
                                                                        
                                                                        redisClient.set(userName, data, function (err, res) { });
                                                                    }
                                                                });
                                                                //End Redis Write
                                                                req.session.userId = item._id;
                                                            });  
                                                            res.json({status:"Login Successful"});                                             
                                                        }else{
                                                            res.json({status:"Login failed"});  
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    }
                                    else {
                                        //Read All Data From MongoDB. There is no record on Redis.
                                        var counter = 0;
                                        User.find({},function (err, doc) {
                                            if(doc.length > 0){
                                                doc.forEach(function (item) {
                                                    counter += 1;
                                                    //Write All Data To Redis
                                                    var userName = 'User' + counter;
                                                    redisClient.get(userName, function (err, user) {
                                                        if (user == null) {
                                                            var data = JSON.stringify(item._doc);
                                                            
                                                            redisClient.set(userName, data, function (err, res) { });
                                                        }
                                                    });
                                                    //End Redis Write
                                                    req.session.userId = item._id;
                                                });
                                                return {status:"Login Successful"};
                                            }else{
                                                res.json({status:"Login failed"})
                                            }
                                        })
                                    }
                                });  
                            }
                            else //If there is no connection to Redis, get all data from MongoDB.
                            {      
                                User.findOne({username:JSON.parse(msg.content.toString()).username,password:JSON.parse(msg.content.toString()).password},function (err, doc) {
                                    //res.send(doc);
                                    if(doc){
                                        req.session.userId = JSON.parse(doc)._id;
                                        return {status:"Login Successful"};
                                    }else{
                                        res.json({status:"Login failed"})
                                    }
                                })
                            }
                        }, {
                            noAck: true
                        });
                    });
                });
			
		},

		
		 logout(req,res) {
			
                if (req.session) {
                    // delete session object
                    req.session.destroy(function (err) {
                        if (err) {
                            return next(err);
                        } else {
                            res.json({status:"Logout"})  
                        }
                    });
                }
			
		},

		
		 addProduct(req,res) {
			
                
                amqp.connect('amqp://rabbitmq', function(error0, connection) {
                    if (error0) {
                        throw error0;
                    }
                    connection.createChannel(function(error1, channel) {
                        if (error1) {
                        throw error1;
                        }
                        var queue = 'addProduct';
                        var msg = req.body;
            
                        channel.assertQueue(queue, {
                        durable: false
                        });
            
                        channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
                        
                        channel.consume(queue, function(msg) {
                            if(req.session.userId){
                                User.findOne({_id:req.session.userId},function(err,doc){
                                    User.findOneAndUpdate({_id:req.session.userId},{products:[...doc.products,{name:JSON.parse(msg.content.toString()).name,price:JSON.parse(msg.content.toString()).price}]},{useFindAndModify: false},function(err,data){
                                        if(err){
                                            res.json({status:"Login First"}); 
                                        }
                                        if(data){
                                            res.json({status:"Added to Cart"});
                                        }
                                    });
                                })   
                            }else{
                                res.json({status:"Login First"});
                            }
                        }, {
                            noAck: true
                        });
                    });
                });
			
		},

		
	    products(req,res) {

                if(req.session.userId){
                    User.findOne({_id:req.session.userId},function(err,data){
                        if(!data.products){
                            this.cartEmpty; 
                        }else{
                            res.json({status: data.products})
                        }
                    });
                }else{
                    res.json({status:"Login First"});
                }
            
			}
        },
    }